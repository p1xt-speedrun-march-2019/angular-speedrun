[![pipeline status](https://gitlab.com/p1xt-speedrun-march-2019/angular-speedrun/badges/master/pipeline.svg)](https://gitlab.com/p1xt-speedrun-march-2019/angular-speedrun/commits/master)
[![pipeline status](https://gitlab.com/p1xt-speedrun-march-2019/angular-speedrun/badges/master/coverage.svg)](https://gitlab.com/p1xt-speedrun-march-2019/angular-speedrun/commits/master)

# AngularSpeedrun

- [ ] Clone [this theme](https://blackrockdigital.github.io/startbootstrap-landing-page/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://blackrockdigital.github.io/startbootstrap-agency/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://blackrockdigital.github.io/startbootstrap-resume/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://blackrockdigital.github.io/startbootstrap-stylish-portfolio/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://webthemez.com/preview/?ballet-one-page-free-website-template/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://webthemez.com/demo/delta-corporate-material-design-bootstrap-html-template/index.html), include all features (hover effects, animations, pages, etc).
- [ ] Clone [this theme](https://blackrockdigital.github.io/startbootstrap-creative/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://blackrockdigital.github.io/startbootstrap-coming-soon/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://webthemez.com/preview/?lookup-interior-design-bootstrap-website-template/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://webthemez.com/demo/flavour-restaurant-html5-responsive-web-template/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://webthemez.com/demo/fine-best-app-landing-page-free-web-template/), include all features (hover effects, animations, pages, etc)
- [ ] Clone [this theme](https://webthemez.com/demo/startup-multi-purpose-responsive-html5-bootstrap-template/), include all features (hover effects, animations, pages, etc)

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
