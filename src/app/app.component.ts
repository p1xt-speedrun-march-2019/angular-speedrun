import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  opened = false;
  title = 'angular-speedrun-march2019';
  routes = [
    {
      name: 'Home',
      link: ''
    }
  ];
}
